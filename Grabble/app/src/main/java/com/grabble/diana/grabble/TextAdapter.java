package com.grabble.diana.grabble;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Layout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;


class TextAdapter extends BaseAdapter {

    Context context;
    List<String> ItemsList;

    HashMap<String,Integer> hashMap;

    public TextAdapter(Context context, List<String> ItemsList) {
        this.context = context;
        this.ItemsList = ItemsList;
        hashMap=new HashMap<>();
        putScoresMap();
    }

    @Override
    public int getCount() {

        return ItemsList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub

        return ItemsList.get(position);
    }

    @Override
    public long getItemId(int position) {

        // TODO Auto-generated method stub

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        TextView text = new TextView(this.context);

        text.setText(Html.fromHtml(ItemsList.get(position)+"<sub>"+hashMap.get(ItemsList.get(position))+""+"</sub>"));

        text.setGravity(Gravity.CENTER);

        //text.setBackgroundColor(Color.parseColor("#9F9F9F"));
        text.setBackgroundResource(R.drawable.piece);

        text.setTextColor(Color.parseColor("#040404"));

        text.setTextSize(16f);
        text.setGravity(Gravity.CENTER);

        text.setLayoutParams(new GridView.LayoutParams(144, 144));

        return text;

    }

    public void putScoresMap()
    {
        hashMap.put("A",3);
        hashMap.put("B",20);
        hashMap.put("C",13);
        hashMap.put("D",10);
        hashMap.put("E",1);
        hashMap.put("F",15);
        hashMap.put("G",18);
        hashMap.put("H",9);
        hashMap.put("I",5);
        hashMap.put("J",25);
        hashMap.put("K",22);
        hashMap.put("L",11);
        hashMap.put("M",14);
        hashMap.put("N",6);
        hashMap.put("O",4);
        hashMap.put("P",19);
        hashMap.put("Q",24);
        hashMap.put("R",8);
        hashMap.put("S",7);
        hashMap.put("T",2);
        hashMap.put("U",12);
        hashMap.put("V",21);
        hashMap.put("W",17);
        hashMap.put("X",23);
        hashMap.put("Y",16);
        hashMap.put("Z",26);
    }

}

