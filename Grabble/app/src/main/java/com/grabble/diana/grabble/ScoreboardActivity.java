package com.grabble.diana.grabble;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class ScoreboardActivity extends Activity {

    private GetPreferences classicScores;
    private GetPreferences riddleScores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        classicScores = new GetPreferences(this.getApplicationContext(),"ClassicScores");
        riddleScores = new GetPreferences(this.getApplicationContext(),"RiddleScores");

        //classicScores.clear();


        TableLayout cTable = (TableLayout) findViewById(R.id.score_classic);
        TableLayout rTable = (TableLayout) findViewById(R.id.score_riddle);

        ArrayList<Integer> classicList = new ArrayList<>();
        ArrayList<Integer> riddleList = new ArrayList<>();

        Log.d("CLASSIC--" , classicScores.getAll().size()+"");
        Log.d("RIDDLE--" , riddleScores.getAll().size()+"");

        if(classicScores.getAll().size()>0)
            for(int i=0;i<classicScores.getAll().size();i++)
             if(classicScores.getInt(i+"")!=0)
                classicList.add(classicScores.getInt(i+""));

        if(riddleScores.getAll().size()>0)
            for(int i=0;i<riddleScores.getAll().size();i++)
             if(riddleScores.getInt(i+"")!=0)
                riddleList.add(riddleScores.getInt(i+""));
        Collections.sort(classicList);
        Collections.sort(riddleList);

        Log.d("LISTA-RI ", riddleList.size()+"");
        Log.d("LISTA-CL ", classicList.size()+"");


        for(int i=classicList.size()-1;i>=0;i--)
        {
            TextView et = new TextView(this.getApplicationContext());
            et.setText(classicList.get(i)+ " ");
            et.setTextSize(24);
            et.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            et.setAlpha(1);
            cTable.addView(et);
        }

        for(int i=riddleList.size()-1;i>=0;i--)
        {
            TextView et = new TextView(this.getApplicationContext());
            et.setTextSize(24);
            et.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            et.setAlpha(1);
            et.setText(riddleList.get(i)+ " ");
            rTable.addView(et);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.icon_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.menu_icon) {
//            preferences = new GetPreferences(this.getApplicationContext(), "GameState");
//            preferences.putString("state", "resume");
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        finish();
    }
}
