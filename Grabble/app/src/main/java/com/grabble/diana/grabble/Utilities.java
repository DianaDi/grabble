package com.grabble.diana.grabble;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by Diana on 20/01/2017.
 */

public class Utilities {

    Context myContext;

    public Utilities(Context myContext)
    {
        this.myContext=myContext;

    }

    public int loadLetter(String s) {
        int re = 0;

        switch (s) {

            case "A": {
                re = R.drawable.a;
                break;
            }
            case "B": {
                re = R.drawable.b;
                break;
            }
            case "C": {
                re = R.drawable.c;
                break;
            }
            case "D": {
                re = R.drawable.d;
                break;
            }
            case "E": {
                re = R.drawable.e;
                break;
            }
            case "F": {
                re = R.drawable.f;
                break;
            }
            case "G": {
                re = R.drawable.g;
                break;
            }
            case "H": {
                re = R.drawable.h;
                break;
            }
            case "I": {
                re = R.drawable.i;
                break;
            }
            case "J": {
                re = R.drawable.j;
                break;
            }
            case "K": {
                re = R.drawable.k;
                break;
            }
            case "L": {
                re = R.drawable.l;
                break;
            }
            case "M": {
                re = R.drawable.m;
                break;
            }
            case "N": {
                re = R.drawable.n;
                break;
            }
            case "O": {
                re = R.drawable.o;
                break;
            }
            case "P": {
                re = R.drawable.p;
                break;
            }
            case "Q": {
                re = R.drawable.q;
                break;
            }
            case "R": {
                re = R.drawable.r;
                break;
            }
            case "S": {
                re = R.drawable.s;
                break;
            }
            case "T": {
                re = R.drawable.t;
                break;
            }
            case "U": {
                re = R.drawable.u;
                break;
            }
            case "V": {
                re = R.drawable.v;
                break;
            }
            case "W": {
                re = R.drawable.w;
                break;
            }
            case "X": {
                re = R.drawable.x;
                break;
            }
            case "Y": {
                re = R.drawable.y;
                break;
            }
            case "Z": {
                re = R.drawable.z;
                break;
            }

        }
        return re;
    }

    public String getDictionary() {
        String text = "dictionary.txt"; //text file name in the assets folder
        String dictionary;

        byte[] buffer = null;

        InputStream is;
        try {
            is = myContext.getAssets().open(text);
            int size = is.available(); //size of the file in bytes
            buffer = new byte[size]; //declare the size of the byte array with size of the file
            is.read(buffer);
            is.close();

        } catch (IOException e) {
            e.printStackTrace();

        }
        // Store text file data in the string variable
        dictionary = new String(buffer);

        return dictionary;

    }

    public HashMap putScoresMap() {

        HashMap hashMap=new HashMap();

        hashMap.put("A", 3);
        hashMap.put("B", 20);
        hashMap.put("C", 13);
        hashMap.put("D", 10);
        hashMap.put("E", 1);
        hashMap.put("F", 15);
        hashMap.put("G", 18);
        hashMap.put("H", 9);
        hashMap.put("I", 5);
        hashMap.put("J", 25);
        hashMap.put("K", 22);
        hashMap.put("L", 11);
        hashMap.put("M", 14);
        hashMap.put("N", 6);
        hashMap.put("O", 4);
        hashMap.put("P", 19);
        hashMap.put("Q", 24);
        hashMap.put("R", 8);
        hashMap.put("S", 7);
        hashMap.put("T", 2);
        hashMap.put("U", 12);
        hashMap.put("V", 21);
        hashMap.put("W", 17);
        hashMap.put("X", 23);
        hashMap.put("Y", 16);
        hashMap.put("Z", 26);

        return hashMap;
    }

    public boolean isNetworkAvailable(Context context) {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec
                =(ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() ==
                android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            return true;
        }else if (
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() ==
                                android.net.NetworkInfo.State.DISCONNECTED  ) {
            return false;
        }
        return false;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

}
