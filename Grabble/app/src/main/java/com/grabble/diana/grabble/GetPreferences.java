package com.grabble.diana.grabble;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import java.util.Map;

public class GetPreferences {

    private static GetPreferences store;
    private SharedPreferences SP;
    private String filename;

    public GetPreferences(Context context,String filename) {
        this.filename=filename;
        SP = context.getSharedPreferences(filename,0);

    }

    public GetPreferences getInstance(Context context, String filename) {
         this.filename=filename;
        if (store == null) {
            Log.v("Keystore","NEW STORE");
            store = new GetPreferences(context,filename);
        }
        Log.v("Keystore", "NOT NEW");

        return store;
    }

    public void putString(String key, String value) {//Log.v("Keystore","PUT "+key+" "+value);
        Editor editor;

        editor = SP.edit();
        editor.putString(key, value);
       // editor.commit(); // Stop everything and do an immediate save!
        editor.apply(); //Keep going and save when you are not busy - Available only in APIs 9 and above.  This is the preferred way of saving.
    }

    public String getString(String key) {//Log.v("Keystore","GET from "+key);
        return SP.getString(key, "start");

    }

    public int getInt(String key) {//Log.v("Keystore","GET INT from "+key);
        return SP.getInt(key, 0);
    }

    public void putInt(String key, int num) {//Log.v("Keystore","PUT INT "+key+" "+String.valueOf(num));
        Editor editor;
        editor = SP.edit();

        editor.putInt(key, num);
        editor.commit();
    }

    public Map<String, ?> getAll()
    {
        return SP.getAll();
    }


    public void clear(){
        Editor editor;
        editor = SP.edit();

        editor.clear();
        editor.commit();
    }

    public void remove(){
        Editor editor;
        editor = SP.edit();

        editor.remove(filename);
        editor.commit();
    }
}