package com.grabble.diana.grabble;

import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.kml.KmlLayer;
import com.google.maps.android.kml.KmlPlacemark;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;


/**
 * Created by Diana on 04/12/2016.
 */

public class MapSetup {

    public MapActivity mapActivity;

    public MapSetup(MapActivity target)
    {
        mapActivity = target;
    }

    public void setUpMap() {
        ((SupportMapFragment) mapActivity.getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(mapActivity);
    }

    public void mapReady (GoogleMap map) {
        if (mapActivity.mMap != null) {
            return;
        }
        mapActivity.mMap = map;

        mapActivity.preferences = new GetPreferences(this.mapActivity.getApplicationContext(), "GameState");
        if (mapActivity.preferences.getString("mode").equals("night")) {
            mapActivity.mMap.setMapStyle(new MapStyleOptions(mapActivity.getResources()
                    .getString(R.string.style_json)));
            mapActivity.currentMode = "night";
        }

        mapActivity.mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker arg0) {

                String titleMarker = "nothing";

                if(arg0.getTitle()!=null) {
                    titleMarker = "I am here!";
                    if(!arg0.getTitle().equals("I am here!")) titleMarker="nothing";
                }




                if (titleMarker.equals("nothing")){
                    if (mapActivity.latLng != null)
                    {

                        double latNow = mapActivity.latLng.latitude;
                        double lngNow = mapActivity.latLng.longitude;
                        double latMarker = arg0.getPosition().latitude;
                        double lngMarker = arg0.getPosition().longitude;

                        Location origin = new Location(LocationManager.GPS_PROVIDER);
                        origin.setLatitude(latNow);
                        origin.setLongitude(lngNow);

                        Location dest = new Location(LocationManager.GPS_PROVIDER);
                        dest.setLatitude(latMarker);
                        dest.setLongitude(lngMarker);

                        float distance = origin.distanceTo(dest);

                        Log.d("DISTANTA ", distance+" ");


                        if(distance<=30)    // the condition of grabbing a letter to be maximum 30 meters away
                        {
                            mapActivity.ItemsList.add(arg0.getSnippet());
                            mapActivity.gridView.setAdapter(new TextAdapter(mapActivity, mapActivity.ItemsList)); // not okay to call multiple times
                            Toast.makeText(mapActivity, "Letter " + arg0.getSnippet() + " was collected", Toast.LENGTH_SHORT).show();// display toast
                            arg0.setVisible(false);
                            
                            mapActivity.collectedMarkers.add(arg0.getPosition()+"");

                       }
                         else Toast.makeText(mapActivity, "You should get closer to collect the letter", Toast.LENGTH_SHORT).show();

                    } else
                        Toast.makeText(mapActivity, "Could not detect your location. Please make sure your GPS is on.", Toast.LENGTH_SHORT).show();

                }
                return false;

            }

        });
        startShow();
    }

    public void startShow() {
        try {
            mapActivity.mMap = getMap();
            //mapActivity.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(55.944005, -3.189169), 17));
            retrieveFileFromUrl();
        } catch (Exception e) {
            Log.e("Exception caught", e.toString());
        }
    }

    public void retrieveFileFromUrl() {

        String dayName=mapActivity.dayLongName;


        if (dayName.equals("Sunday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Sunday)).execute();
        else if (dayName.equals("Monday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Monday)).execute();
        else if (dayName.equals("Tuesday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Tuesday)).execute();
        else if (dayName.equals("Wednesday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Wednesday)).execute();
        else if (dayName.equals("Thursday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Thursday)).execute();
        else if (dayName.equals("Friday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Friday)).execute();
        else if (dayName.equals("Saturday"))
            new DownloadKmlFile(mapActivity.getString(R.string.kml_Saturday)).execute();


    }

    public class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        public DownloadKmlFile(String url) {
            mUrl = url;
        }

        protected byte[] doInBackground(String... params) {
            try {
                InputStream is = new URL(mUrl).openStream();

                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[1024];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }

                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                KmlLayer kmlLayer = new KmlLayer(mapActivity.mMap, new ByteArrayInputStream(byteArr),
                        mapActivity.getApplicationContext());


                for (Iterator<KmlPlacemark> iter = kmlLayer.getPlacemarks().iterator(); iter.hasNext(); ) {
                    KmlPlacemark p = (KmlPlacemark) iter.next();
                    LatLng pointL = getLocation(p.getGeometry().getGeometryObject().toString());
                    String letter = p.getProperty("description");
                    MarkerOptions options = new MarkerOptions()
                            .position(pointL)
                            .icon(BitmapDescriptorFactory.fromResource(mapActivity.util.loadLetter(letter)))
                            .snippet(letter);
                    mapActivity.mMap.addMarker(options);

                }

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public GoogleMap getMap() {
        return mapActivity.mMap;
    }


    public LatLng getLocation(String s) {
        int i, j;
        for (i = 0; i < s.length(); i++)
            if (s.charAt(i) == ('(')) break;
        for (j = i; j < s.length(); j++)
            if (s.charAt(j) == (')')) break;

        String[] latLng = s.substring(i + 1, j).split(",");
        double latitude = Double.parseDouble(latLng[0]);
        double longitude = Double.parseDouble(latLng[1]);
        LatLng location = new LatLng(latitude, longitude);


        return location;
    }


}
