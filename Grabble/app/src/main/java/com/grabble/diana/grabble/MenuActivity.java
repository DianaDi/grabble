package com.grabble.diana.grabble;

import android.app.Activity;
import android.app.Service;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends Activity {

    private GetPreferences preferences;

    public Utilities util;
    public String string;
    public TextView img1;
    public TextView img2;
    public TextView txt1;
    public TextView txt2;

    public Button play_classic;
    public Button play_riddle;
    public Button end_classic;
    public Button end_riddle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.menu_layout);

        preferences = new GetPreferences(MenuActivity.this.getApplicationContext(),"GameState");
        preferences.getInstance(MenuActivity.this.getApplicationContext(),"GameState");

        util = new Utilities(getApplicationContext());

        //preferences.clear();

        play_classic = (Button) findViewById(R.id.play_classic);
        play_riddle = (Button) findViewById(R.id.play_riddle);
        end_classic = (Button) findViewById(R.id.classic_end);
        end_riddle = (Button) findViewById(R.id.riddle_end);


            if (preferences.getString("state").equals("start"))
            {
                play_classic.setText("PLAY");
                play_riddle.setText("PLAY");
                end_classic.setVisibility(View.INVISIBLE);
                end_riddle.setVisibility(View.INVISIBLE);
            }
            else {
                if (preferences.getString("game").equals("riddle")) {
                    play_riddle.setText("RESUME");
                    end_riddle.setVisibility(View.VISIBLE);
                    end_classic.setVisibility(View.INVISIBLE);
                    play_classic.setEnabled(false);}

                if (preferences.getString("game").equals("classic")){

                    play_classic.setText("RESUME");
                    end_classic.setVisibility(View.VISIBLE);
                    end_riddle.setVisibility(View.INVISIBLE);
                    play_riddle.setEnabled(false);

                }
            }

        img1 = (TextView) findViewById(R.id.day_image);
        img2 = (TextView) findViewById(R.id.night_image);
        txt1 = (TextView) findViewById(R.id.day_mode);
        txt2 = (TextView) findViewById(R.id.night_mode);

        if (preferences.getString("mode").equals("night")) {
            img2.setAlpha(1);
            txt2.setAlpha(1);
            img1.setAlpha(.3f);
            txt1.setAlpha(.3f);
        }
        else
        {
            img2.setAlpha(.3f);
            txt2.setAlpha(.3f);
            img1.setAlpha(1);
            txt1.setAlpha(1);
            preferences.putString("mode","day");
        }

        preferences.putString("state", "start");

    }



    public void startGame(View v)  //OR RESUME GAME
    {
        if(util.isNetworkAvailable(MenuActivity.this)) {
            if (util.isLocationEnabled(MenuActivity.this)) {



                if (v.getId() == R.id.play_classic) {
                    preferences.putString("game", "classic");
                    if (play_classic.getText().equals("PLAY")) {
                        Intent intent = new Intent(this, MapActivity.class);
                        startActivity(intent);
                        super.finish();
                    } else if (play_classic.getText().equals("RESUME"))
                    {
                        Intent intent = new Intent();
                        intent.putExtra("flag", true);
                        setResult(RESULT_OK, intent);
                        super.finish();
                    }
                }
                else {
                    preferences.putString("game", "riddle");
                    if (play_riddle.getText().equals("PLAY")) {
                        Intent intent = new Intent(this, MapActivity.class);
                        startActivity(intent);
                        super.finish();
                    } else if (play_riddle.getText().equals("RESUME")) {
                        Intent intent = new Intent();
                        intent.putExtra("flag", true);
                        setResult(RESULT_OK, intent);
                        super.finish();
                    }

                }

            }
            else fToast("Could not detect location. Please turn Location Services on.");

        }
        else fToast("Could not connect to internet. Please check your connection and try again.");
    }

    public void endGame(View v)
    {
        Intent intent = new Intent();
        intent.putExtra("flag", true);
        setResult(RESULT_CANCELED, intent);

        preferences.putString("state","start");
        play_classic.setText("PLAY");
        play_riddle.setText("PLAY");
        end_classic.setVisibility(View.INVISIBLE);
        end_riddle.setVisibility(View.INVISIBLE);
        play_riddle.setEnabled(true);
        play_classic.setEnabled(true);

    }

    public void nightMode(View v)
    {
        preferences.putString("mode","night");
        img2.setAlpha(1);
        txt2.setAlpha(1);
        img1.setAlpha(.3f);
        txt1.setAlpha(.3f);
    }

    public void dayMode(View v)
    {
        preferences.putString("mode","day");
        img2.setAlpha(.3f);
        txt2.setAlpha(.3f);
        img1.setAlpha(1);
        txt1.setAlpha(1);
    }

    public void scoreBoard(View v)
    {
        v.setAlpha(0.4f);
        Intent i = new Intent(this, ScoreboardActivity.class);
        startActivity(i);
        v.setAlpha(1f);
    }


    private void fToast(String s) {
        //Context context = getApplicationContext();
        //int duration = Toast.LENGTH_LONG;


        // get your custom_toast.xml ayout
        LayoutInflater inflater = getLayoutInflater();

        View layout = inflater.inflate(R.layout.toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        // Toast...
        Toast toast = new Toast(getApplicationContext());

        // Toast toast = Toast.makeText(context, s, duration);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(s+" ");
        toast.setGravity(Gravity.BOTTOM, 0, 20);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);

        toast.show();
    }


    @Override
    public void finish()
    {
        preferences.putString("state", "start");
        super.finish();

    }


    @Override
    public void onBackPressed()
    {

    }

}
