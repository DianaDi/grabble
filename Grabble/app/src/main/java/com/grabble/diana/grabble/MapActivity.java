package com.grabble.diana.grabble;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;



public class MapActivity extends FragmentActivity implements OnMapReadyCallback, LocationProvider.LocationCallback {


    LocationProvider mLocationProvider;
    GoogleMap mMap;
    private MapSetup mapSetup;

    GetPreferences preferences, scores, riddles;

    GridView gridView;
    ScrollView scrollView;
    TextView score, timer, wordProgress;
    Button lettersGrid, showWords, riddle, cancel_word, add_word;

    String selectedItem;
    String wordBefore = "";
    String currentMode = "day";
    String gameMode = " ";
    String dayLongName;
    String dictionary;
    String currentRiddle = "unsolved";

    final static int REQUEST_CODE_A = 1;
    int scoreValue = 0, numberRiddle = 0, numberOfScores = 0;
    double currentLatitude, currentLongitude;
    LatLng latLng;

    boolean wrongWord = true, hintGiven = false, timeUp = false;

    HashMap<String, Integer> hashMap;
    Map<String, ?> riddlesHashMap;
    List<String> ItemsList;
    ArrayList<String> riddlesAnswers;
    ArrayList<String> itemsName = new ArrayList<String>();
    ArrayList<String> collectedMarkers;


    CountDownTimer miau;
    AlertDialog.Builder currentBuilder;
    AlertDialog currentDialog;

    MarkerOptions options;
    Marker marker;
    Utilities util;

    Location mLastLocation;
    Marker mCurrLocationMarker;
    Boolean menuStart = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationProvider = new LocationProvider(this, this);

        util = new Utilities(this.getApplicationContext());

        dictionary = util.getDictionary(); // load the dictionary from txt file


        initializeGameMode();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(MapActivity.this);

        mapSetup = new MapSetup(this);
        mapSetup.setUpMap();



    }

    ///////////// INITIALIZE
    public void initializeGameMode()
    {


        preferences = new GetPreferences(this.getApplicationContext(), "GameState");
        collectedMarkers = new ArrayList<>();


        // load the markers


        if (preferences.getString("game").equals("classic")) {
            gameMode = "classic";
            setContentView(R.layout.activity_classic);
            scores = new GetPreferences(this.getApplicationContext(), "ClassicScores");
            scrollView = (ScrollView) findViewById(R.id.words_list);
            scrollView.setVisibility(View.INVISIBLE);
            showWords = (Button) findViewById(R.id.show_words);
            showWords.setVisibility(View.INVISIBLE);

        } else if (preferences.getString("game").equals("riddle")) {
            gameMode = "riddle";
            setContentView(R.layout.activity_riddle);
            scores = new GetPreferences(this.getApplicationContext(), "RiddleScores");
            riddle = (Button) findViewById(R.id.id_riddle);
            riddle.setVisibility(View.INVISIBLE);
            timer = (TextView) findViewById(R.id.timer);
            timer.setVisibility(View.INVISIBLE);
            loadRiddles();
        }


        hashMap = new HashMap<>();
        hashMap= util.putScoresMap();

        if (!(scores.getAll().size() + "").equals(null)) numberOfScores = scores.getAll().size();

        score = (TextView) findViewById(R.id.score);
        score.setVisibility(View.INVISIBLE);

        lettersGrid = (Button) findViewById(R.id.id_show);
        lettersGrid.setVisibility(View.INVISIBLE);

        cancel_word = (Button) findViewById(R.id.cancel_word);
        add_word = (Button) findViewById(R.id.add_word);
        wordProgress = (TextView) findViewById(R.id.word_inProgress);

        cancel_word.setVisibility(View.INVISIBLE);
        add_word.setVisibility(View.INVISIBLE);
        wordProgress.setVisibility(View.INVISIBLE);

        Calendar sCalendar = Calendar.getInstance();
        dayLongName = sCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.UK);

        ItemsList = itemsName;
        gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new TextAdapter(this, ItemsList));
        gridViewOnClickListener();
    }

    ///////////// MAP
    @Override
    public void onMapReady(GoogleMap map)
    {
        mapSetup.mapReady(map);
        if (mMap != null) {
            return;
        }
        mMap = map;
    }

    public void handleNewLocation(Location location) {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
//            latLng = new LatLng(currentLatitude, currentLongitude);
//
//            if(options==null) {
//                //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
//                 options = new MarkerOptions()
//                        .position(latLng)
//                        .title("I am here!")
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.smiley_happy));
//                 marker = mMap.addMarker(options);
//                 mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
//            }
//
//            else marker.setPosition(latLng);
//
//        }
//        else {
//            Toast.makeText(MapActivity.this, "Could not detect your location. Please make sure your GPS is on.", Toast.LENGTH_SHORT).show();
//        }

            Boolean animate = false;

            mLastLocation = location;
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            else animate = true;

            //Place current location marker

            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("I am here!");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.smiley_happy));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            if(animate) mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));




        }
    }

    /////////// ACTION BAR
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_A: {
                switch (resultCode)

                {
                    case RESULT_OK: {
                        menuStart = false;
                        preferences = new GetPreferences(this.getApplicationContext(), "GameState");

                        if (preferences.getString("mode").equals("night") && currentMode.equals("day")) {
                            currentMode = "night";
                            mMap.setMapStyle(new MapStyleOptions(getResources().getString(R.string.style_json)));
                        } else if (preferences.getString("mode").equals("day") && currentMode.equals("night")) {
                            currentMode = "day";
                            mMap.setMapStyle(new MapStyleOptions("[]"));
                        }

                        break;
                    }
                    case RESULT_CANCELED: {

                        finish();
                        break;
                    }
                }
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.icon_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.menu_icon) {
            preferences = new GetPreferences(this.getApplicationContext(), "GameState");
            preferences.putString("state", "resume");

            menuStart = true;
            scores.putInt(numberOfScores + "", scoreValue);

            Intent intent = new Intent(this, MenuActivity.class);
            startActivityForResult(intent, REQUEST_CODE_A);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    ////////// GAME
    public void gridViewOnClickListener() {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                selectedItem = parent.getItemAtPosition(position).toString();

                if (wordBefore.equals("")) {
                    showWordWindow();
                    wordBefore = selectedItem;
                    wordProgress.setText(wordBefore);
                } else {
                    wordProgress.setText(wordBefore + selectedItem);
                    wordBefore = wordBefore + selectedItem;
                }

                for (int i = 0; i < ItemsList.size(); i++)
                    if (ItemsList.get(i).equals(selectedItem)) {
                        ItemsList.remove(i);
                        break;
                    }

                gridView.setAdapter(new TextAdapter(MapActivity.this, ItemsList)); // not okay to call multiple times

            }
        });

    }

    public void makeLettersVisible(View v) {
        if (gridView.getVisibility() == View.INVISIBLE) gridView.setVisibility(View.VISIBLE);
        else gridView.setVisibility(View.INVISIBLE);

    }

    public void cancelWord(View v) {
        wordProgress.setText("");
        wordProgress.setVisibility(View.INVISIBLE);
        cancel_word.setVisibility(View.INVISIBLE);
        add_word.setVisibility(View.INVISIBLE);

        if (!wordBefore.equals(""))
            for (int i = 0; i < wordBefore.length(); i++)
                ItemsList.add(wordBefore.charAt(i) + "");
        gridView.setAdapter(new TextAdapter(MapActivity.this, ItemsList)); // not okay to call multiple times

        wordBefore = "";

    }

    public void testWord(View v) {

        if (gameMode.equals("classic")) {
            String lines[] = dictionary.split("\\r?\\n");
            boolean found = false;
            for (int i = 0; i < lines.length; i++)
                if (lines[i].toLowerCase().equals(wordProgress.getText().toString().toLowerCase())) {
                    LinearLayout mLayout = (LinearLayout) findViewById(R.id.miau);
                    TextView et = new TextView(this.getApplicationContext());
                    et.setText(wordProgress.getText().toString());
                    et.setTextSize(20);
                    et.setTextColor(Color.BLACK);
                    et.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    mLayout.addView(et);
                    correctWordEntered();
                    found = true;
                    break;
                }
            if (!found)
                Toast.makeText(getApplicationContext(), "The word entered is not found in the Grabble Dictionary. Try again!", Toast.LENGTH_SHORT).show();
        } else {
            if (riddlesAnswers.get(numberRiddle).toLowerCase().equals(wordProgress.getText().toString().toLowerCase())) {
                Toast.makeText(getApplicationContext(), "Great!", Toast.LENGTH_SHORT).show();
                if (numberRiddle < 1)
                    Toast.makeText(getApplicationContext(), "YOU WON THE GAME!", Toast.LENGTH_SHORT).show();
                else {
                    numberRiddle--;
                    correctWordEntered();
                    miau.cancel();
                    timer.setText(" ");
                    newTimer();
                    currentRiddle = "unsolved";
                    showRiddleDialog();
                }
            } else
                Toast.makeText(getApplicationContext(), "Not the right word. Try again!", Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelWordRiddle(View v) {
        wordProgress.setText("");
        wordProgress.setVisibility(View.INVISIBLE);
        cancel_word.setVisibility(View.INVISIBLE);
        add_word.setVisibility(View.INVISIBLE);

        if (!wordBefore.equals(""))
            for (int i = 0; i < wordBefore.length(); i++)
                ItemsList.add(wordBefore.charAt(i) + "");
        gridView.setAdapter(new TextAdapter(MapActivity.this, ItemsList)); // not okay to call multiple times

        wordBefore = "";

    }

    public void testWordRiddle(View v) {

        if (gameMode.equals("classic")) {
            String lines[] = dictionary.split("\\r?\\n");
            boolean found = false;
            for (int i = 0; i < lines.length; i++)
                if (lines[i].toLowerCase().equals(wordProgress.getText().toString().toLowerCase())) {
                    LinearLayout mLayout = (LinearLayout) findViewById(R.id.miau);
                    TextView et = new TextView(this.getApplicationContext());
                    et.setText(wordProgress.getText().toString());
                    et.setTextSize(20);
                    et.setTextColor(Color.BLACK);
                    et.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    mLayout.addView(et);
                    correctWordEntered();
                    found = true;
                    break;
                }
            if (!found)
                Toast.makeText(getApplicationContext(), "The word entered is not found in the Grabble Dictionary. Try again!", Toast.LENGTH_SHORT).show();
        } else {
            if(currentRiddle.equals("solved")) {
                if (riddlesAnswers.get(numberRiddle).toLowerCase().equals(wordProgress.getText().toString().toLowerCase())) {
                    Toast.makeText(getApplicationContext(), "Great!", Toast.LENGTH_SHORT).show();
                    if (numberRiddle < 1)
                        Toast.makeText(getApplicationContext(), "YOU WON THE GAME!", Toast.LENGTH_SHORT).show();
                    else {
                        numberRiddle--;
                        correctWordEntered();
                        miau.cancel();
                        timer.setText(" ");
                        newTimer();
                        currentRiddle = "unsolved";
                        showRiddleDialog();
                    }
                } else
                    Toast.makeText(getApplicationContext(), "Not the right word. Try again!", Toast.LENGTH_SHORT).show();
            }
            else Toast.makeText(getApplicationContext(), "You have to solve the riddle first, and then form the word", Toast.LENGTH_SHORT).show();
        }
    }

    public void correctWordEntered() {
        for (int j = 0; j < wordBefore.length(); j++) {
            scoreValue = scoreValue + hashMap.get(wordBefore.charAt(j) + "");
            score.setText("Score \n" + scoreValue + "");
        }

        cancel_word.setVisibility(View.INVISIBLE);
        add_word.setVisibility(View.INVISIBLE);
        wordProgress.setText("");
        wordBefore = "";
        wordProgress.setVisibility(View.INVISIBLE);
    }

    public void showWordWindow() {
        cancel_word.setVisibility(View.VISIBLE);
        add_word.setVisibility(View.VISIBLE);
        wordProgress.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(util.isNetworkAvailable(MapActivity.this)) {
            if (util.isLocationEnabled(MapActivity.this)) {
                mapSetup.setUpMap();
                mLocationProvider.connect();
            } else Toast.makeText(this, "Could not detect location. Please turn Location Services on.", Toast.LENGTH_SHORT).show();

        }
        else Toast.makeText(this, "Could not connect to internet. Please check your connection and try again.", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPause() {
        if(!menuStart) preferences.putString("state","start");
        super.onPause();
        mLocationProvider.disconnect();
    }

    @Override
    public void finish()
    {
        scores.putInt(numberOfScores+"",scoreValue);
        preferences.putString("state","start");
        if(gameMode.equals("riddle")) riddles.clear();
        super.finish();
    }


    public void StartGame (View v) {
                v.setVisibility(View.INVISIBLE);
                score.setVisibility(View.VISIBLE);
                lettersGrid.setVisibility(View.VISIBLE);

                if(gameMode.equals("classic")) showWords.setVisibility(View.VISIBLE);

                else
                {
                    timer.setVisibility(View.VISIBLE);
                    riddle.setVisibility(View.VISIBLE);

                    newTimer();

                    riddles = new GetPreferences(this.getApplicationContext(), "Riddles");
                    riddlesHashMap = riddles.getAll();
                    riddlesAnswers = new ArrayList<>();
                    for (Map.Entry<String, ?> entry : riddlesHashMap.entrySet())
                        riddlesAnswers.add(entry.getKey());
                    numberRiddle = riddlesAnswers.size() - 1;

                    showRiddleDialog();

                }

            }

    public void makeWordsVisible(View v) {
        if (scrollView.getVisibility() == View.VISIBLE) scrollView.setVisibility(View.INVISIBLE);
        else scrollView.setVisibility(View.VISIBLE);
    }

    ////////// RIDDLE GAME
    public void showRiddle(View v) {
        if(currentRiddle.equals("solved")) showRiddleSolvedDialog();
            else showRiddleDialog();
    }

    public void loadRiddles() {
        riddles = new GetPreferences(this.getApplicationContext(),"Riddles");

        if(preferences.getString("riddleGame").equals("2"))
             {
            preferences.putString("riddleGame","1");

            riddles.putString("Nothing", "\nWhat is greater than God, More evil than the devil,\nThe poor have it,\nThe rich don't need it,\nAnd if you eat it, you'll die?");
            riddles.putString("Silence", "\nWhat disappears the moment you say its name?");
            riddles.putString("Billion", "\nI am a 7 Letter word\n" +
                    "\nMost Humans want me.\n" +
                    "But they hate the first 4 letters of my name.\n" +
                    "If you get the 2nd, 3rd and 4th letter you are sick.\n" +
                    "The 5th, 6th and 7th letter are something with charge.\n" +
                    "Who am I ?");
            riddles.putString("Keyhole", "\nWhat goes through a door but never goes in and never comes out?");
            riddles.putString("History", "You will always find me in the past. \nI can be created in the present,\nBut the future can never taint me. \nWhat am I?");
             }

        else {
            preferences.putString("riddleGame","2");

            riddles.putString("Stuffed", "\nTeddy bears are never hungry because they are always what?");
            riddles.putString("Catfish", "\nWhat kind of fish chases a mouse?");
            riddles.putString("Sawdust", "\nWhat is made of wood, but can't be sawed?");
            riddles.putString("History", "\nSometimes I am liked, \nSometimes I am hated. \nUsually I am old,\n Usually I am dated.");
            riddles.putString("Thimble", "\nAn open ended barrel, I am shaped like a hive. I am filled with the flesh, and the flesh is alive! What am I?");
        }
    }

    public void newTimer()
    {
        miau = new CountDownTimer(600000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText(("TIME LEFT \n " + millisUntilFinished / 60000) + ":" + (millisUntilFinished % 60000 / 1000));
            }

            public void onFinish() {
                timer.setText("Time is up!");
                timeUp=true;
                hintGiven=false;
                currentRiddle = "unsolved";
                showTimeUpDialog();


            }
        }.start();
    }

    ///////// DIALOGS
    public void showTimeUpDialog() {

        currentDialog.cancel();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_timeup, null);
        dialogBuilder.setView(dialogView);


        final TextView riddle_text = (TextView) dialogView.findViewById(R.id.riddle_text);
        final TextView answer = (TextView) dialogView.findViewById(R.id.answer_done);
        final TextView messageTimeup = (TextView) dialogView.findViewById(R.id.message_timeup);

        answer.setText(riddlesAnswers.get(numberRiddle));
        riddle_text.setText(riddlesHashMap.get(riddlesAnswers.get(numberRiddle)) + "");

        if (numberRiddle >= 1) {
            timeUp=true;
            messageTimeup.setText("Time is up! You lost this riddle challenge! Here is the answer. ");
            dialogBuilder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //do something with edt.getText().toString();
                    numberRiddle--;
                    miau.cancel();
                    timer.setText(" ");
                    miau = new CountDownTimer(600000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            timer.setText(("TIME LEFT \n " + millisUntilFinished / 60000) + ":" + (millisUntilFinished % 60000 / 1000));
                        }

                        public void onFinish() {
                            timer.setText("Time is up!");
                            timeUp=true;
                            hintGiven=false;
                            showTimeUpDialog();

                        }
                    }.start();

                    timeUp=false;
                    showRiddleDialog();

                }
            });
        }

        else {
            miau.cancel();
            timer.setText(" ");
            timeUp=true;
            hintGiven=false;
            messageTimeup.setText("Time is up! You lost this riddle challenge! Here is the answer. \n This game is over! You can go to the menu and start another one.");
            dialogBuilder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent intent = new Intent(MapActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();

                }
            });
        }

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showRiddleDialog() {


        currentBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_riddle, null);
        currentBuilder.setView(dialogView);



        final EditText edt = (EditText) dialogView.findViewById(R.id.answer);
        edt.setHintTextColor(Color.BLACK);
        final TextView riddle_text = (TextView) dialogView.findViewById(R.id.riddle_text);
        final TextView hint_text = (TextView) dialogView.findViewById(R.id.hint_text);

        riddle_text.setText(riddlesHashMap.get(riddlesAnswers.get(numberRiddle)) + "");


        if (!hintGiven) {
            currentBuilder.setPositiveButton("Hint", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    hint_text.setText("The first two letters are: " + riddlesAnswers.get(numberRiddle).substring(0, 2).toUpperCase());
                    hint_text.setTextSize(18);
                    hintGiven = true;
                    showRiddleDialog();

                }

            });

        } else {

            hint_text.setText("The first two letters are: " + riddlesAnswers.get(numberRiddle).substring(0, 2).toUpperCase());
            hint_text.setTextSize(18);

            currentBuilder.setPositiveButton("Give up!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    currentRiddle = "unsolved";
                    hintGiven = false;

                    timer.setText(" ");
                    miau.cancel();
                    wrongWord=false;
                    showGiveUpRiddleDialog();
                }

            });
        }

        currentBuilder.setNeutralButton("Test answer", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();


                if (edt.getText().toString().toLowerCase().equals(riddlesAnswers.get(numberRiddle).toLowerCase())) {
                    Toast.makeText(MapActivity.this, "Riddle is solved! Now form the word!", Toast.LENGTH_SHORT).show();
                    currentRiddle = "solved";
                    wrongWord = false;
                } else {
                    Toast.makeText(getApplicationContext(), "Not the right word. Try again!", Toast.LENGTH_SHORT).show();
                    wrongWord = true;
                    showRiddleDialog();

                }

            }
        });


        currentBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });

        currentDialog = currentBuilder.create();
        currentDialog.show();


    }

    public void showRiddleSolvedDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_riddle_solved, null);
        dialogBuilder.setView(dialogView);

        final TextView answer = (TextView) dialogView.findViewById(R.id.answer);
        final TextView riddle_text = (TextView) dialogView.findViewById(R.id.riddle_text);

        riddle_text.setText(riddlesHashMap.get(riddlesAnswers.get(numberRiddle)) + "");
        answer.setText(riddlesAnswers.get(numberRiddle));

        dialogBuilder.setNeutralButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showGiveUpRiddleDialog() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_riddle_giveup, null);
        dialogBuilder.setView(dialogView);

        final TextView answer = (TextView) dialogView.findViewById(R.id.answer);
        final TextView riddle_text = (TextView) dialogView.findViewById(R.id.riddle_text);
        final TextView giveup_message = (TextView) dialogView.findViewById(R.id.giveup_message);

        riddle_text.setText(riddlesHashMap.get(riddlesAnswers.get(numberRiddle)) + "");
        answer.setText(riddlesAnswers.get(numberRiddle));


        if (numberRiddle >= 1) {
            giveup_message.setText("You lost this riddle challenge! Here is the answer. ");
            dialogBuilder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //do something with edt.getText().toString();
                    numberRiddle--;
                    miau.cancel();
                    timer.setText(" ");
                    timeUp=false;
                    miau = new CountDownTimer(600000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            timer.setText(("TIME LEFT \n " + millisUntilFinished / 60000) + ":" + (millisUntilFinished % 60000 / 1000));
                        }

                        public void onFinish() {
                            timer.setText("Time is up!");
                            timeUp=true;
                            hintGiven=false;
                            showTimeUpDialog();

                        }
                    }.start();
                    showRiddleDialog();
                }
            });
        }

        else {
            giveup_message.setText("You lost this riddle challenge! Here is the answer. \n This game is over! You can go to the menu and start another one.");
            dialogBuilder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent intent = new Intent(MapActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();

                }
            });
        }

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public void onBackPressed()
    {

    }




}